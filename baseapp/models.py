from django.db import models
from django.contrib.auth.models import User
import datetime

# Create your models here.
class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    user_type = models.BooleanField(max_length=20, default=True)

    def __str__(self):
        return self.user.username

    def getUserType(self):
        return self.user_type

class Customer(models.Model):
    user_profile = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    b_day = models.DateField( ("Date"), default=datetime.date.today, null = True)
    points = models.IntegerField(default = '0', null = True)
    loc_range = models.IntegerField(default = '3', null = True)
    pic = models.ImageField(upload_to='consumer_pic', default='consumer_pic/consumer.png', null = True)
    tag = models.TextField(default='', blank = True)

    def __str__(self):
        return self.user_profile.user.username

class Business(models.Model):
    user_profile = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    pic = models.ImageField(upload_to='business_pic', default='business_pic/business.png', null = True)
    company_name = models.CharField(max_length=40, default='DEFAULT VALUE')
    branch_name = models.CharField(max_length=40, default='DEFAULT VALUE')
    address = models.CharField(max_length=100 , default='DEFAULT VALUE')
    location = models.CharField(max_length=100 , default='DEFAULT VALUE')


    def __str__(self):
        return self.user_profile.user.username + self.company_name

class Food(models.Model):
    business = models.ForeignKey(Business, on_delete=models.CASCADE)
    name = models.CharField(max_length=20, default='DEFAULT VALUE')
    pic = models.ImageField(upload_to='food_pic', default='food_pic/food.png', null=True)
    price = models.FloatField(default = '0')
    tag = models.TextField(default='', blank=True)
    desc = models.TextField(default='', blank=True)

    def __str__(self):
        return self.business.user_profile.user.username + "::" +self.name

class Ratings(models.Model):
    food_id = models.IntegerField()
    user_id = models.IntegerField()
    rating = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)


class Photo(models.Model):
    file = models.ImageField(upload_to='uploaded_pic')

    class Meta:
        verbose_name = 'photo'
        verbose_name_plural = 'photos'

class ConsumerPhoto(models.Model):
    file_file = models.ImageField(upload_to='consumer_pic')

    class Meta:
        verbose_name = 'photo'
        verbose_name_plural = 'photos'

class BusinessPhoto(models.Model):
    file = models.ImageField(upload_to='business_pic')

    class Meta:
        verbose_name = 'photo'
        verbose_name_plural = 'photos'

class FoodPhoto(models.Model):
    file = models.ImageField(upload_to='food_pic')

    class Meta:
        verbose_name = 'photo'
        verbose_name_plural = 'photos'

class Advertisements(models.Model):
    name = models.CharField(max_length=20, default='DEFAULT VALUE')
    pic = models.ImageField(upload_to='Advertisements', default='', null=True)
    price = models.FloatField(default = '0')
    tag = models.TextField(default='', blank=True)
    desc = models.TextField(default='', blank=True)
    url = models.TextField(default='', blank=True)


class Rewards(models.Model):
    name = models.CharField(max_length=20, default='DEFAULT VALUE')
    pic = models.ImageField(upload_to='Rewards', default='', null=True)
    points = models.FloatField(default = '0')


class FoodForUpload(models.Model):
    consumer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    food_id = models.IntegerField()

class CommunityPic(models.Model):
    food = models.ForeignKey(Food, on_delete=models.CASCADE)
    url = models.TextField(default='', blank=True)




