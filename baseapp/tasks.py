from django.utils.timezone import timedelta
from celery.decorators import periodic_task


@periodic_task(run_every=(timedelta(minutes=2)), name="remodel_ai", ignore_result=True)
def remodel_ai():
    from baseapp.apps import aimodel
    if(aimodel == None):
        pass
    else:
        from utilities.foodai import initialize_model
        initialize_model()
        # from utilities.foodai import rating_remodel
        # rating_remodel()