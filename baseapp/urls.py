from django.urls import path, include
from django.conf.urls import url, include

from . import views

app_name = 'baseapp'

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^login/$', views.LoginView.as_view(), name='login'),
    url(r'^signup-consumer/$', views.SignUpConsumerView.as_view(), name='sign-up-consumer'),
    url(r'^signup-business/$', views.SignUpBusinessView.as_view(), name='sign-up-business'),
    url(r'^business/dashboard/$', views.DashboardView.as_view(), name='dashboard'),
    url(r'^business/dashboard/location/$', views.DashboardLocation.as_view(), name='location'),
    url(r'^business/add-product/$', views.AddProductView.as_view(), name='add_product'),
    url(r'^business/chart-data/$', views.ChartDataView.as_view(), name='chart-data'),
    url(r'^business/products-list/$', views.ProductsListView.as_view(), name='products_list'),
    url(r'^consumer/feed/$', views.FeedView.as_view(), name='feed'),
    url(r'^consumer/feed/money/$', views.FeedAdvertisement.as_view(), name='advertisement'),
    url(r'^consumer/feed/points/$', views.FeedPoints.as_view(), name='points'),
    url(r'^consumer/feed/food-upload/$', views.FeedFoodUpload.as_view(), name='food-upload'),
    url(r'^consumer/feed/loc-range/$', views.LocationRangeView.as_view(), name='location-range'),
    url(r'^consumer/feed/community-pic/$', views.CommunityPicView.as_view(), name='community-pic'),
    url(r'^consumer/profile/$', views.ProfileView.as_view(), name='profile'),
    url(r'^consumer/feed/suggestions/', views.FoodRequest.as_view(), name='suggestions'),
    url(r'^consumer/feed/rate/', views.FoodRate.as_view(), name='rate'),
    url(r'^consumer/feed/pref-tags/', views.FoodPref.as_view(), name='pref-tags'),

    url(r'^populate', views.Populate.as_view()),
]