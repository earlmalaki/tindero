from django.contrib.auth.models import User
from .models import User, UserProfile, Customer, Business, Food, Ratings, Rewards, Advertisements
from django.core import serializers
import json

class PopulateApp():

    def createBusinessBulk():
        businessStatic = [
            {
                'company_name': 'Jollibee',
                'branch_name': 'Jollibee The Space',
                'address': 'A.S. Fortuna, Mandaue City, Cebu'
            },
            {
                'company_name': 'Chowking',
                'branch_name': 'Chowking Gaisano Country Mall',
                'address': 'Banilad, Cebu City'
            },
            {
                'company_name': 'Mcdo',
                'branch_name': 'Mcdo i2 Bldg IT Park',
                'address': 'IT Park, Apas, Cebu City'
            },
            {
                'company_name': 'Greenwich',
                'branch_name': 'Greenwich Ayala',
                'address': 'Ayala, Business Park, Cebu City'
            }
        ]

        # BUSINESS
        for i in range(len(businessStatic)):
            user = User(
                username="fakebusiness"+str(i),
                password="pbkdf2_sha256$100000$ejksMWJqcm5F$MO59T+4YBoK2rZ6X4ceGY2PrFRycFNisOzf4ID/eJTs=",
                email="info@example.com"
            )
            user.save()
            userP = UserProfile(user = user, user_type = False)
            userP.save()

            business = Business(
                user_profile=userP,
                company_name=businessStatic[i]['company_name'],
                branch_name=businessStatic[i]['branch_name'],
                address=businessStatic[i]['address']
                )
            business.save()

    def createConsumerBulk():
        # CONSUMER
        for i in range(35):
            user = User(
                username="fakeconsumer"+str(i),
                password="pbkdf2_sha256$100000$ejksMWJqcm5F$MO59T+4YBoK2rZ6X4ceGY2PrFRycFNisOzf4ID/eJTs=",
                email="info@example.com"
                )
            user.save()
            userP = UserProfile(user = user, user_type = True)
            userP.save()

            consumer = Customer(user_profile = userP)
            consumer.save()


    def addProductsToBusinesses():

        # 1 jollibee | foodsjollibee.json
        # 2 Chowking | foodschowking.json
        # 3 Mcdo | foodsmcdo.json
        # 4 Greenwich | foodsgreenwich.json

        busiFood = [
            {
                "pk": 1,
                "filename": "foodsjollibee.json"
            },
            {
                "pk": 2,
                "filename": "foodschowking.json"
            },
            {
                "pk": 3,
                "filename": "foodsmcdo.json"
            },
            {
                "pk": 4,
                "filename": "foodsgreenwich.json"
            },
        ]

        for i in range(len(busiFood)):
            current_business = Business.objects.get(pk=busiFood[i]['pk'])
            print(current_business)

            with open(busiFood[i]['filename'], "r") as file:
                data = json.loads(file.read())

                for i in range(len(data)):

                    list_of_tags = data[i]['food_tags']

                    tags = ",".join(list_of_tags)

                    food = Food(
                        business = current_business,
                        name = data[i]['food_name'],
                        price = data[i]['food_price'],
                        tag = tags,
                        desc = data[i]['food_desc']
                        )
                    food.save()


    def generateFixture():
        # user = serializers.serialize("json", User.objects.all(), indent=2)
        # with open('fixture_users.json', 'w') as file:
        #     file.write(user)

        # user_profile = serializers.serialize("json", UserProfile.objects.all(), indent=2)
        # with open('fixture_user_profile.json', 'w') as file:
        #     file.write(user_profile)

        # consumer = serializers.serialize("json", Customer.objects.all(), indent=2)
        # with open('fixture_consumer.json', 'w') as file:
        #     file.write(consumer)

        # business = serializers.serialize("json", Business.objects.all(), indent=2)
        # with open('fixture_business.json', 'w') as file:
        #     file.write(business)

        # food = serializers.serialize("json", Food.objects.all(), indent=2)
        # with open('fixture_food.json', 'w') as file:
        #     file.write(food)

        # ratings = serializers.serialize("json", Ratings.objects.all(), indent=2)
        # with open('fixture_ratings.json', 'w') as file:
        #     file.write(ratings)

        # rewards = serializers.serialize("json", Rewards.objects.all(), indent=2)
        # with open('fixture_rewards.json', 'w') as file:
        #     file.write(rewards)

        advertisements = serializers.serialize("json", Advertisements.objects.all(), indent=2)
        with open('fixture_advertisements.json', 'w') as file:
            file.write(advertisements)














