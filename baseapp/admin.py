from django.contrib import admin

# Register your models here.
from .models import UserProfile, Customer, Business, Food, Ratings, Photo, Advertisements, Rewards, FoodForUpload, CommunityPic

admin.site.register(UserProfile)
admin.site.register(Customer)
admin.site.register(Business)
admin.site.register(Food)
admin.site.register(Ratings)
admin.site.register(Photo)
admin.site.register(Advertisements)
admin.site.register(Rewards)
admin.site.register(FoodForUpload)
admin.site.register(CommunityPic)
