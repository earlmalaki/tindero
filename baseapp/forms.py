from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth.models import User

from django.core.files import File

from .models import UserProfile, Business, Customer, Food, FoodPhoto, BusinessPhoto, ConsumerPhoto, Photo, FoodForUpload, CommunityPic
from PIL import Image

class NameForm(forms.Form):
    your_name = forms.CharField(label='Your name', max_length = 100)

class RegisterForm(UserCreationForm):
    username = forms.CharField(
        widget=forms.TextInput(attrs={'id':'username','class': 'input is-rounded', 'type':'text', 'placeholder': 'username', 'name':'username', 'v-model':'username'}))
    email=forms.EmailField(
        widget=forms.EmailInput(attrs={'id':'email','class': 'input is-rounded','type':'email', 'placeholder': 'dummy123@gmail.com', 'name':'email', 'v-model':'email'}), max_length=64, help_text='Enter a valid email address')
    password1=forms.CharField(
        widget=forms.PasswordInput(attrs={'id':'password1','class': 'input is-rounded','type':'password', 'placeholder': 'password'}))
    password2=forms.CharField(
        widget=forms.PasswordInput(attrs={'id':'password2','class': 'input is-rounded','type':'password', 'placeholder': 'confirm password'}))
    userType=forms.CharField(
        widget=forms.TextInput(attrs={'id':'user_type','class': 'input is-rounded', 'type':'text', 'placeholder': 'username', 'name':'username', 'v-model':'accountType', 'readonly':"readonly"}))


    class Meta:
        model = User
        fields = ("username","password1","password2", "email", "userType")

    def clean_username(self):
        # Since User.username is unique, this check is redundant,
        # but it sets a nicer error message than the ORM. See #13147.
        username = self.cleaned_data["username"]
        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'])

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1", "")
        password2 = self.cleaned_data["password2"]
        if password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'])
        return password2

    def save(self):

        type_of_user = self.cleaned_data["userType"]
        user_type = True

        if (type_of_user == "business account"):
            user_type = False
        else:
            user_type = True

        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        user.save()

        userP = UserProfile(user = user, user_type = user_type)
        userP.save()

        return user

class BusinessForm(forms.Form):
    company_name = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'input is-rounded', 'type':'text','placeholder':'company name'}))
    branch_name = forms.CharField(
        widget=forms.TextInput(attrs={'id':'branch_name','class': 'input is-rounded', 'type':'text','placeholder':'branch name'}))
    # branch_address = forms.CharField(
    #     widget=forms.TextInput(attrs={'class': 'input is-rounded', 'type':'text','placeholder':'branch location'}))

    def save(self):
        userP = UserProfile.objects.latest('user_id')

        business = Business(
            user_profile = userP,
            branch_name = self.cleaned_data["branch_name"],
            company_name = self.cleaned_data["company_name"]
        )

        business.save()

class ConsumerForm(forms.Form):

    def save(self):
        userP = UserProfile.objects.latest('user_id')
        consumer = Customer(user_profile = userP)
        consumer.save()

class FoodForm(forms.Form):
    food_name = forms.CharField(
        widget=forms.TextInput(attrs={
                                    'class':'input',
                                    'type':'text',
                                    'placeholder':'food name',
                                    'name':'foodname',
                                    'v-validate':'\'required\'',
                                    'v-model':'foodName'}))
    food_price = forms.FloatField(
        widget=forms.TextInput(attrs={
                                    'class':'input',
                                    'type':'text',
                                    'placeholder':'price',
                                    'name':'price',
                                    'v-validate':'\'required|decimal\'',
                                    'v-model':'price'}))

    food_description = forms.CharField(
        widget=forms.Textarea(attrs={
                                    'class':'textarea desc-text-area',
                                    'placeholder':'place yummy product description here',
                                    'rows':'8',
                                    'name':'description',
                                    'v-validate':'\'required|alpha_spaces|max:100\'',
                                    'v-model':'description'}))

    food_tags = forms.CharField(
        widget=forms.TextInput(attrs={
                                    'id':'foodtags',
                                    'class':'input',
                                    'type':'text',
                                    'placeholder':'put accurate tags to imporve discoverability',
                                    'name':'tags',
                                    'v-model':'tagTextField',
                                    'v-on:keydown.enter':'addTag'}))


    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(FoodForm, self).__init__(*args, **kwargs)

    def save(self):

        current_user = self.user
        user_profile = UserProfile.objects.get(user = current_user)
        #it would be better if we could save the User.id as a field in UserProfile
        print(Business.objects.get(user_profile = user_profile))
        current_business = Business.objects.get(user_profile = user_profile)
        food = Food(
            business = current_business,
            name = self.cleaned_data["food_name"],
            price = self.cleaned_data["food_price"],
            tag = self.cleaned_data["food_tags"],
            desc = self.cleaned_data["food_description"]
        )
        food.save()


class EditProfileForm(forms.Form):
    first_name = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'input is-rounded', 'type':'text','placeholder':'name'}))
    bDay = forms.DateField(label='Birthday',
        widget=forms.SelectDateWidget(years=[y for y in range(1930,2050)]))

    def clean_name(self):
        return self.cleaned_data["first_name"]

    def clean_bday(self):
        return self.cleaned_data["bDay"]

class PhotoConsumerForm(forms.ModelForm):
    x = forms.FloatField(widget=forms.HiddenInput())
    y = forms.FloatField(widget=forms.HiddenInput())
    width = forms.FloatField(widget=forms.HiddenInput())
    height = forms.FloatField(widget=forms.HiddenInput())

    class Meta:
        model = ConsumerPhoto
        fields = ('file_file', 'x', 'y', 'width', 'height',)

    def save(self):
        photo = super(PhotoConsumerForm, self).save()

        x = self.cleaned_data.get('x')
        y = self.cleaned_data.get('y')
        w = self.cleaned_data.get('width')
        h = self.cleaned_data.get('height')

        image = Image.open(photo.file_file)
        cropped_image = image.crop((x, y, w+x, h+y))
        resized_image = cropped_image.resize((128,128), Image.ANTIALIAS)
        resized_image.save(photo.file_file.path)

        return photo

class PhotoBusinessForm(forms.ModelForm):
    x = forms.FloatField(widget=forms.HiddenInput())
    y = forms.FloatField(widget=forms.HiddenInput())
    width = forms.FloatField(widget=forms.HiddenInput())
    height = forms.FloatField(widget=forms.HiddenInput())

    class Meta:
        model = BusinessPhoto
        fields = ('file', 'x', 'y', 'width', 'height',)

    def save(self):
        photo = super(PhotoBusinessForm, self).save()

        x = self.cleaned_data.get('x')
        y = self.cleaned_data.get('y')
        w = self.cleaned_data.get('width')
        h = self.cleaned_data.get('height')

        image = Image.open(photo.file)
        cropped_image = image.crop((x, y, w+x, h+y))
        resized_image = cropped_image.resize((128,128), Image.ANTIALIAS)
        resized_image.save(photo.file.path)

        return photo

class PhotoFoodForm(forms.ModelForm):
    x = forms.FloatField(widget=forms.HiddenInput())
    y = forms.FloatField(widget=forms.HiddenInput())
    width = forms.FloatField(widget=forms.HiddenInput())
    height = forms.FloatField(widget=forms.HiddenInput())

    class Meta:
        model = FoodPhoto
        fields = ('file', 'x', 'y', 'width', 'height',)

    def save(self):
        photo = super(PhotoFoodForm, self).save()

        x = self.cleaned_data.get('x')
        y = self.cleaned_data.get('y')
        w = self.cleaned_data.get('width')
        h = self.cleaned_data.get('height')

        image = Image.open(photo.file)
        cropped_image = image.crop((x, y, w+x, h+y))
        resized_image = cropped_image.resize((960,720), Image.ANTIALIAS)
        resized_image.save(photo.file.path)

        return photo

class PhotoUploadForm(forms.ModelForm):
    x = forms.FloatField(widget=forms.HiddenInput())
    y = forms.FloatField(widget=forms.HiddenInput())
    width = forms.FloatField(widget=forms.HiddenInput())
    height = forms.FloatField(widget=forms.HiddenInput())
    object_id = forms.IntegerField(widget=forms.HiddenInput())

    class Meta:
        model = Photo
        fields = ('file','x', 'y', 'width', 'height')

    def save(self):
        photo = super(PhotoUploadForm, self).save()

        x = self.cleaned_data.get('x')
        y = self.cleaned_data.get('y')
        w = self.cleaned_data.get('width')
        h = self.cleaned_data.get('height')

        image = Image.open(photo.file)
        cropped_image = image.crop((x, y, w+x, h+y))
        resized_image = cropped_image.resize((800,800), Image.ANTIALIAS)
        resized_image.save(photo.file.path)

        food_on_modal_upload = FoodForUpload.objects.get(pk=self.cleaned_data['object_id'])
        food_on_modal_upload.delete()

        # create new community_pic
        cur_food = Food.objects.get(pk=food_on_modal_upload.food_id)
        new_community_pic = CommunityPic(food=cur_food, url="/media/" + str(photo.file))
        new_community_pic.save()

        return photo