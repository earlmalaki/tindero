// Note: Permission to locate is required.
var serviceDistMatrix, currPos, servicePlaces, geocoder, infoWindow, map;

function initGeoLoc() {

    geocoder = new google.maps.Geocoder;
    serviceDistMatrix = new google.maps.DistanceMatrixService();

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(geoPos) {
            var currPos = {
                lat: geoPos.coords.latitude,
                lng: geoPos.coords.longitude
            };

            infoWindow.setPosition(currPos);
            map.setCenter(currPos);
            geocodeLatLng(currPos);
        },
        function() {
            handleLocationError(true, infoWindow, map.getCenter());
        });
      } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, map.getCenter());
      }
  }

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 15,
        disableDefaultUI: true,
        clickableIcons: false,
        gestureHandling: "none",
        styles: [
        ]
    });

    infoWindow = new google.maps.InfoWindow;
    infoWindow.setContent("You're here!");
    infoWindow.open(map);
    servicePlaces = new google.maps.places.PlacesService(map);

    initGeoLoc();
}

function geocodeLatLng(currPos) {
    console.log("geocodeLatLng()");
    geocoder.geocode({'location': currPos}, function(results, status) {
        if (status === 'OK') {
            if (results[0]) {
                app.userAddress = results[0].formatted_address;
                app.userLocation = currPos;
                app.initializeCardFeed();
            } else {
                window.alert('No results found');
            }
        } else {
            window.alert('Geocoder failed due to: ' + status);
        }
    });
}


function getDistances(origins, destinations) {
    console.log("getDistance()");

    return new Promise(function(resolve, reject) {
        serviceDistMatrix.getDistanceMatrix(
            {
                origins: origins,
                destinations: destinations,
                travelMode: 'DRIVING',
                unitSystem: google.maps.UnitSystem.METRIC,
                avoidHighways: false,
                avoidTolls: false,
            },
            function(response, status) {
                if (status !== 'OK') {
                    reject(status);
                } else {
                    resolve(response.rows[0]);
                }
          });
    });
}


function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
}

