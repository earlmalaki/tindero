

// Note: Permission to locate is required.
var map, infoWindow, service, autocomplete, geoPos;
var selectedPlace, markers = [];

// Set up geolocation services
// Set center of map to user location
function initGeolocate() {
    if (navigator.geolocation) {
        // Browser supports Geolocation
        navigator.geolocation.getCurrentPosition(function(geoPosition) {
            var currPos = {
                lat: geoPosition.coords.latitude,
                lng: geoPosition.coords.longitude
            };

            infoWindow.setPosition(currPos);
            map.setCenter(currPos);
            initAutocomplete(geoPosition);

            app.isLoading = false;

            if ( (app.selectedLocation == "DEFAULT VALUE") || ((app.selectedAddress == "DEFAULT VALUE")) ) {
                app.openLocSelModal();
            }
        },
        function() {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }
}

function initMap() {
    map = new google.maps.Map(document.getElementById('map-area'), {
        zoom: 15,
        styles: [
        ]
    });

    infoWindow = new google.maps.InfoWindow;
    infoWindow.setContent("You're here!");
    infoWindow.open(map);
    service = new google.maps.places.PlacesService(map);

    google.maps.event.addListener(map, 'bounds_changed', function() {
        console.log("map bounds_changed")
        if (autocomplete != null && autocomplete.getPlace() != null) {
            searchNearby(autocomplete.getPlace().name);
        }
    });

    initGeolocate();
}

// set up search bar autocompletion based on user's location
function initAutocomplete(geoPosition) {
    var input = document.getElementById('locSearchTextField');
    var circle = new google.maps.Circle({
        center: map.getCenter(),
        radius: geoPosition.coords.accuracy
    });
    var options = {
        bounds: circle.getBounds()
    };
    autocomplete = new google.maps.places.Autocomplete(input, options);
    autocomplete.addListener('place_changed', placeChanged);
    autocomplete.bindTo('bounds', map);
}

// Function to call everytime the user selects a suggested place
function placeChanged() {
    clearMarkers();
    console.log("placeChanged()");
    var place = autocomplete.getPlace();

    if (place.address_components == null) {
        console.log(`No predefined place selected. Will searchNearby instead with query = ${place.name}`)
        searchNearby(place.name);
    } else {
        // User selected a valid place from suggestions
        console.log(`suggestion selected.`)
        map.setCenter(place.geometry.location)
        setBusiVars(place);
        markSelectedPlace(app.selectedAddress);
    }
}

function searchNearby(placeKeyword) {
    console.log("searchNearby()");
    // Search nearby places based on placeKeyword
    // Search is bounded to the visible area on the map
    var request = {
        bounds: map.getBounds(),
        name: placeKeyword,
    };
    service.nearbySearch(request, markResults);
}

function markSelectedPlace(address) {
    var request = {
        query: address,
        fields: ['formatted_address', 'name', 'geometry'],
    }
    service.findPlaceFromQuery(request, markResults);
}

function markResults(results, status) {
    console.log("markResults")
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        for (var i = 0; i < results.length; i++) {
            createMarker(results[i]);
        }
    }
}

function createMarker(place) {
    var marker = new google.maps.Marker({
        map: map,
        position: place.geometry.location
    });

    markers.push(marker);

    google.maps.event.addListener(marker, 'click', function() {
        console.log("marker selected")
        app.selectedLocation = place.geometry.location;
        if (place.formatted_address == null) {
            getDetailsMarker(place.place_id);
        } else {
            setBusiVars(place);
        }
    });
}

function clearMarkers() {
    for (var i = 0; i <= markers.length - 1; i++) {
        markers[i].setMap(null);
    }
}

function getDetailsMarker(place_id) {
    console.log(`getDM ${place_id}`)
    var request = {
        placeId: place_id,
    };

    service.getDetails(request, function(place) {
        setBusiVars(place);
    });
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
}

function setBusiVars(place) {
    console.log("setBusiVars()")
    console.log(place)
    console.log(place.formatted_address)
    app.selectedAddress = place.formatted_address;
    app.selectedLocation = `${place.geometry.location.lat()},${place.geometry.location.lng()}` ;
}