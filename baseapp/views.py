import json
from utilities.foodai import get_suggestions
from datetime import date, timedelta


from django.shortcuts import render, render_to_response,redirect
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth import login, logout, authenticate
from django.template import RequestContext
from django.core import serializers
from django.http import HttpResponse, JsonResponse

from .forms import RegisterForm, BusinessForm, ConsumerForm, FoodForm, PhotoUploadForm, EditProfileForm, PhotoConsumerForm, PhotoBusinessForm, PhotoFoodForm
from .models import User, UserProfile, Customer, Business, Food, Ratings, Photo, FoodPhoto, BusinessPhoto, ConsumerPhoto, Advertisements, Rewards, FoodForUpload, CommunityPic

from .bulkCreateUsers import PopulateApp

from baseapp.signals import trigger_recompute


# Create your views here.
class IndexView(View):

    def get(self,request):

        if request.user.is_authenticated:
            if request.user.userprofile.user_type:
                return redirect('baseapp:feed')
            else:
                return redirect('baseapp:dashboard')

        register_form = RegisterForm()
        authentication_form = AuthenticationForm()
        business_form = BusinessForm()
        consumer_form = ConsumerForm()

        authentication_form.fields['username'].widget.attrs['class'] = "input is-rounded"
        authentication_form.fields['username'].widget.attrs['placeholder'] = "username"
        authentication_form.fields['password'].widget.attrs['class'] = "input is-rounded"
        authentication_form.fields['password'].widget.attrs['placeholder'] = "password"
        context = {
                    'register_form': register_form,
                    'consumer_form': consumer_form,
                    'business_form': business_form,
                    'authentication_form': authentication_form,
                }
        return render(request, 'baseapp/index.html', context)

class LoginView(View):

    def post(self, request):

        authentication_form = AuthenticationForm(data=json.loads(request.body.decode('utf-8')))

        if authentication_form.is_valid():
            login(request, authentication_form.get_user())

            if authentication_form.get_user().userprofile.getUserType():
                context = {'status': 'success', 'url': 'consumer/feed/', 'errors':''}
                return JsonResponse(context, safe=False)
            else:
                context = {'status': 'success', 'url': 'business/dashboard/', 'errors':''}
                return JsonResponse(context, safe=False)
        else:
            context = {'status': 'error', 'url': '', 'errors':authentication_form.errors}
            return JsonResponse(context, safe=False)

class SignUpConsumerView(View):

    def post(self, request):
        user_creation_form = UserCreationForm(json.loads(request.body.decode('utf-8')))
        register_form  = RegisterForm(json.loads(request.body.decode('utf-8')))
        consumer_form = ConsumerForm(json.loads(request.body.decode('utf-8')))

        if (user_creation_form.is_valid() and register_form.is_valid() and consumer_form.is_valid()):
            register_form.save()
            consumer_form.save()
            trigger_recompute.send(sender=Customer)

            username = user_creation_form.cleaned_data.get('username')
            raw_password = user_creation_form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            context = {'status': 'success', 'url': 'consumer/feed/', 'errors':''}
            return JsonResponse(context, safe=False)
        else:
            context = {'status': 'error', 'url': '', 'errors':user_creation_form.errors}
            # g = user_creation_form.errors
            return JsonResponse(context, safe=False)

class SignUpBusinessView(View):

    def post(self, request):
        user_creation_form = UserCreationForm(json.loads(request.body.decode('utf-8')))
        register_form  = RegisterForm(json.loads(request.body.decode('utf-8')))
        business_form = BusinessForm(json.loads(request.body.decode('utf-8')))

        if (user_creation_form.is_valid() and register_form.is_valid() and business_form.is_valid()):
            register_form.save()
            business_form.save()

            username = user_creation_form.cleaned_data.get('username')
            raw_password = user_creation_form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            context = {'status': 'success', 'url': 'business/dashboard/', 'errors':''}
            return JsonResponse(context, safe=False)
        else:
            context = {'status': 'error', 'url': '', 'errors':user_creation_form.errors}
            # g = user_creation_form.errors
            return JsonResponse(context, safe=False)


class DashboardView(LoginRequiredMixin, View):


    def get(self, request):
        image_form = PhotoBusinessForm()
        business = Business.objects.get(user_profile=request.user.userprofile)
        food_count = business.food_set.all().count()
        context = {
            'business': business,
            'image_form':image_form,
            'food_count':food_count
        }
        return render(request, 'baseapp/dashboard.html', context)

    def post(self, request):
        image_form = PhotoBusinessForm(request.POST, request.FILES)
        if request.POST.get("logout"):
            logout(request)
            return redirect('baseapp:index')
        elif request.POST.get("save"):
            if image_form.is_valid():
                user = request.user
                business = Business.objects.get(user_profile=user.userprofile)
                image_form.save()
                photo = BusinessPhoto.objects.latest('id')
                business.pic = photo.file
                business.save()
                return redirect('baseapp:dashboard')


class DashboardLocation(LoginRequiredMixin, View):

    def get(self, request):
        current_business = Business.objects.get(
            user_profile=UserProfile.objects.get(
            user=request.user))

        context = {
            'address': current_business.address,
            'location': current_business.location
        }

        return JsonResponse(context, safe=False)

    def post(self, request):
        current_business = Business.objects.get(
            user_profile=UserProfile.objects.get(
            user=request.user))

        data = json.loads(request.body.decode('utf-8'))
        print(data['address'])
        print(data['location'])

        current_business.address = data['address']
        current_business.location = data['location']

        current_business.save()

        return JsonResponse({})

class ChartDataView(LoginRequiredMixin, View):

    def get(self, request):

        business = Business.objects.get(user_profile=request.user.userprofile)
        foods = business.food_set.values_list('id', flat=True)
        ratings = Ratings.objects.filter(food_id__in=foods)
        chart_data = []

        for day in range(7):

            start_day = date.today() - timedelta(days=day)
            end_day = start_day + timedelta(days=1)

            ratings_today = ratings.filter(created_at__gte=start_day).filter(created_at__lt=end_day)

            data = {
                "day": day,
                "nope":ratings_today.filter(rating=-1).count(),
                "yup":ratings_today.filter(rating=0).count(),
                "eat":ratings_today.filter(rating=1).count()
            }

            chart_data.append(data)

        return JsonResponse(chart_data, safe=False)

class AddProductView(LoginRequiredMixin, View):

    def post(self,request):

        current_business = Business.objects.get(
            user_profile=UserProfile.objects.get(
            user=request.user))

        form = FoodForm(request.POST, request.FILES, user=request.user)
        image_form = PhotoFoodForm(request.POST, request.FILES)
        message = ""

        context ={
            'form': form,
            'image_form': image_form,
            'company_name':current_business.company_name,
            'branch_name':current_business.branch_name,
            'location':current_business.location,
            'address':current_business.address,
            'message': message
        }

        if form.is_valid() and image_form.is_valid():
            form.save()
            trigger_recompute.send(sender=Food)
            image_form.save()
            photo = FoodPhoto.objects.latest('id')
            food = Food.objects.latest('id')
            food.pic = photo.file
            food.save()
            form = FoodForm()
            message = "Food saved"
            image_form = PhotoFoodForm()
        else:
            message = "Food not saved"
            print(form.errors)

        return render(request, 'baseapp/add-product.html', context)

    def get(self,request):

        current_business = Business.objects.get(
                user_profile=UserProfile.objects.get(
                user=request.user))

        form = FoodForm()
        image_form = PhotoFoodForm()
        context ={
            'form': form,
            'image_form': image_form,
            'company_name':current_business.company_name,
            'branch_name':current_business.branch_name,
            'location':current_business.location,
            'address':current_business.address
        }

        return render(request, 'baseapp/add-product.html', context)


class ProductsListView(LoginRequiredMixin, View):

    def get(self,request):

        current_business = Business.objects.get(
                user_profile=UserProfile.objects.get(
                user=request.user))

        foods = current_business.food_set.values_list('id','name','price','desc','tag')
        # foods = current_business.food_set.all()

        food_details = []

        for food in foods:

            nope = Ratings.objects.filter(food_id=food[0]).filter(rating=-1).count()
            eat = Ratings.objects.filter(food_id=food[0]).filter(rating=0).count()
            yup = Ratings.objects.filter(food_id=food[0]).filter(rating=1).count()

            context = {
                'food_id': food[0],
                'food_name': food[1],
                'food_price': food[2],
                'food_desc': food[3],
                'food_tag': food[4],
                'nope':nope,
                'eat':eat,
                'yup':yup
            }
            food_details.append(context)

        return render(request, 'baseapp/products-list.html', {'foods':food_details, 'business': current_business})


class FeedView(LoginRequiredMixin, View):

    def get(self,request):
        print(request.user.id)
        consumer = Customer.objects.get(user_profile=request.user.userprofile)
        consumer_image = PhotoConsumerForm()
        image_form = PhotoUploadForm()
        context = {
            'consumer':consumer,
            'user':request.user,
            'image_form': image_form,
            'consumer_image': consumer_image
            }
        return render(request, 'baseapp/consumer-feed.html', context)

    def post(self,request):
        if request.POST.get("logout"):
            logout(request)
            return redirect('baseapp:index')
        elif request.POST.get("save"):
            print("Save");
            image_form = PhotoUploadForm(request.POST, request.FILES)
            if image_form.is_valid():
                image_form.save()

                # food_on_modal_upload = FoodForUpload.objects.get(pk=request.POST.get('object_id'))

                #

                # print(request.POST)
                # food_on_modal_upload.delete()
                consumer = Customer.objects.get(user_profile=request.user.userprofile)
                consumer.points += 5
                consumer.save()
                return redirect('baseapp:feed')
            else:
                print(image_form.errors)
                return HttpResponse(image_form.errors)
        elif request.POST.get("upload"):
            consumer_image = PhotoConsumerForm(request.POST, request.FILES)
            if consumer_image.is_valid():
                consumer_image.save()
                consumer = Customer.objects.get(user_profile=request.user.userprofile)
                photo = ConsumerPhoto.objects.latest('id')
                consumer.pic = photo.file_file
                consumer.save()
                return redirect('baseapp:feed')
            else:
                return HttpResponse(consumer_image.errors)
        else:
            return HttpResponse("HAHA")


class FeedAdvertisement(LoginRequiredMixin, View):

    def get(self, request):
        ads = Advertisements.objects.values_list('name', 'pic', 'price', 'tag', 'desc', 'url')

        advertisements = []
        for ad in ads:
            current_object = {
                'food_id':'',
                'food_place':'',
                'food_location':'',
                'food_name':ad[0],
                'food_pic_url':ad[1],
                'food_price':ad[2],
                'food_tags':ad[3],
                'food_desc':ad[4],
                'link_url':ad[5]
            }
            advertisements.append(current_object)

        return JsonResponse(advertisements, safe=False)



class FeedPoints(LoginRequiredMixin, View):

    def get(self, request):
        rewards_data = list(Rewards.objects.values_list('name','pic','points').values())
        return JsonResponse(rewards_data, safe=False)

    def post(self, request):
        print(json.loads(request.body.decode('utf-8')))
        consumer = Customer.objects.get(user_profile=request.user.userprofile)
        consumer.points = json.loads(request.body.decode('utf-8'))
        consumer.save()

        return JsonResponse({})


class LocationRangeView(LoginRequiredMixin, View):

    def post(self, request):

        data = json.loads(request.body.decode('utf-8'))
        consumer = Customer.objects.get(user_profile=request.user.userprofile)
        consumer.loc_range = data['loc_range']
        consumer.save()
        return JsonResponse({})

class CommunityPicView(LoginRequiredMixin, View):
    def post(self, request):
        current_food = Food.objects.get(pk=json.loads(request.body.decode('utf-8'))['food_id'])
        community_pics = list(current_food.communitypic_set.values_list('url', flat=True))
        return JsonResponse(community_pics, safe=False)

class ProfileView(LoginRequiredMixin, View):

    def get(self,request):
        consumer = Customer.objects.get(user_profile=request.user.userprofile)
        image_form = PhotoConsumerForm(initial={'file':consumer.pic})
        profile_form = EditProfileForm(initial={'first_name': request.user.first_name, 'b_day':consumer.b_day, 'pic':consumer.pic})
        context = {
            'profile_form': profile_form,
            'consumer': consumer,
            'image_form': image_form
        }
        return render(request, 'baseapp/profile.html', context)

    def post(self,request):
        profile_form = EditProfileForm(request.POST, request.FILES)
        image_form = PhotoConsumerForm(request.POST, request.FILES)
        if request.POST.get("logout"):
            logout(request)
            return redirect('baseapp:index')
        elif request.POST.get("save_profile"):
            if profile_form.is_valid() and image_form.is_valid():
                image_form.save()
                user = request.user
                consumer = Customer.objects.get(user_profile=user.userprofile)
                photo = ConsumerPhoto.objects.latest('id')
                user.first_name = profile_form.clean_name()
                consumer.b_day = profile_form.clean_bday()
                consumer.pic = photo.file
                consumer.save()
                user.save()
                return redirect('baseapp:feed')
            else:
                return redirect('baseapp:profile')
        else:
            return redirect('baseapp:profile')


class FoodRequest(LoginRequiredMixin,View):

    def post(self, request):
        print(request)

        current_consumer = Customer.objects.get(
                user_profile=UserProfile.objects.get(
                user=request.user))

        offset = json.loads(request.body.decode('utf-8'))

        if offset > Food.objects.values().count():
            food_query = get_suggestions(current_consumer.id, Food.objects.values().count())
        else:
            food_query = get_suggestions(current_consumer.id, offset)


        print(food_query)
        food_list = []

        for food in food_query:

            current_object = {
                'food_id':food.id,
                'food_place':food.business.branch_name,
                'food_location':food.business.location,
                'food_name':food.name,
                'food_price':str(food.price),
                'food_desc':food.desc,
                'food_tags':food.tag,
                'food_pic_url':food.pic.url
            }

            food_list.append(current_object)

        return JsonResponse(food_list, safe=False)


class FoodRate(LoginRequiredMixin,View):


    def post(self, request):

        j = json.loads(request.body.decode('utf-8'))

        new_rating = Ratings(
            user_id = request.user.id,
            food_id = j['foodID'],
            rating = j['rating']
        )
        new_rating.save()

        if j['rating'] == 1:
            new_upload = FoodForUpload(
                consumer=Customer.objects.get(user_profile=request.user.userprofile),
                food_id = j['foodID'])

            new_upload.save()

            return JsonResponse({"object_id":new_upload.id})

        return JsonResponse({})


class FoodPref(LoginRequiredMixin,View):

    def get(self, request):

        to_JSON = [Customer.objects.get(
                user_profile=UserProfile.objects.get(
                user=request.user)).tag]

        return JsonResponse (to_JSON, safe=False)

    def post(self, request):

        current_consumer = Customer.objects.get(
                user_profile=UserProfile.objects.get(
                user=request.user))

        current_consumer.tag = json.loads(request.body.decode('utf-8'))
        current_consumer.save()
        trigger_recompute.send(sender=Customer)

        return JsonResponse({})

class FeedFoodUpload(LoginRequiredMixin, View):

    def get(self, request):

        consumer = Customer.objects.get(user_profile=request.user.userprofile)

        foods_for_upload = consumer.foodforupload_set.all()

        upload_list = []

        for foods in foods_for_upload:

            current_food = Food.objects.get(pk=foods.food_id)

            current_object = {
                'object_id':foods.id,
                'food_id':current_food.id,
                'food_place':current_food.business.branch_name,
                'food_location':current_food.business.location,
                'food_name':current_food.name,
                'food_price':str(current_food.price),
                'food_desc':current_food.desc,
                'food_tags':current_food.tag,
                'food_pic_url':current_food.pic.url
            }

            upload_list.append(current_object)

        return JsonResponse(upload_list, safe=False)


class Populate(View):
    def get(self, request):
        # PopulateApp.createBusinessBulk()
        # PopulateApp.createConsumerBulk()
        # PopulateApp.addProductsToBusinesses()
        # PopulateApp.generateFixture()
        return HttpResponse("Successfully populated database.")



