from .models import Ratings
from django.db.models.signals import post_save
from django.dispatch import receiver, Signal
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync

trigger_recompute = Signal()

@receiver(post_save, sender=Ratings)
def announce_new_rating(sender, instance, created, **kwargs):
    if created:
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)(
            "ratings_channel",{
                "type": "websocket.receive"
            }
        )

from .models import Customer, Food

@receiver(trigger_recompute, sender=Customer)
@receiver(trigger_recompute, sender=Food)
def recompute_model(sender, **kwargs):
    from utilities.foodai import initialize_model
    from baseapp.apps import aimodel
    
    aimodel = initialize_model()