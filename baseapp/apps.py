from django.apps import AppConfig

aimodel = None
class BaseappConfig(AppConfig):
    name = 'baseapp'

    def ready(self):
        from . import signals
        from utilities.foodai import initialize_model
        global aimodel
        aimodel = initialize_model()