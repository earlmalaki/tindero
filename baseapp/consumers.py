import asyncio
import json
from django.contrib.auth import get_user_model
from channels.consumer import AsyncConsumer
from channels.db import database_sync_to_async
from django.core import serializers

from .models import User, UserProfile, Customer, Business, Food, Ratings


class RatingConsumer(AsyncConsumer):

    async def websocket_connect(self, event):

        await self.send({
            "type": "websocket.accept",
        })

        await self.channel_layer.group_add(
            "ratings_channel",
            self.channel_name
        )
        me = self.scope['user']
        ratings = await self.get_rating(me)

        await self.send({
            "type": "websocket.send",
            "text": json.dumps(ratings)
        })

    async def websocket_receive(self, event):

        me = self.scope['user']
        ratings = await self.get_rating(me)

        await self.send({
            "type": "websocket.send",
            "text": json.dumps(ratings)
        })

    async def websocket_disconnect(self, event):
        print("disconnect", event)

    @database_sync_to_async
    def get_rating(self, user):

       current_business = Business.objects.get(
                user_profile=UserProfile.objects.get(user=user))

       foods = current_business.food_set.values_list('id', flat=True)

       ratings = {
        'nope': Ratings.objects.filter(food_id__in=foods).filter(rating=-1).count(),
        'yup': Ratings.objects.filter(food_id__in=foods).filter(rating=0).count(),
        'eat': Ratings.objects.filter(food_id__in=foods).filter(rating=1).count()
       }
       return ratings


