from baseapp.models import Ratings, Food, Customer

def get_data():

    return (
            Ratings.objects.all(),
            Food.objects.values('id','price','tag'),
            Customer.objects.values('id','tag'),
        )


def get_ratings():

    return get_data()[0]


def get_item_features():

    return get_data()[1]

def get_user_features():

    return get_data()[2]

def create_dataset():

    # test_set_fraction = 0.1 #constant. Training set is 90% of dataset and Test set is 10%

    from lightfm.data import Dataset

    dataset = Dataset()
    dataset.fit((x.user_id for x in get_ratings()),
                (x.food_id for x in get_ratings()))

    dataset.fit_partial(items=(x['id'] for x in get_item_features()),
                         item_features=(x['price'] for x in get_item_features()))

    dataset.fit_partial(users=(x['id'] for x in get_user_features()))

    (interactions, weights) = dataset.build_interactions(((x.user_id, x.food_id, int(x.rating))
                                                        for x in get_ratings()))

    item_price = dataset.build_item_features(((x['id'], [x['price']])
                                                for x in get_item_features()),normalize=True)

    import pandas as pd

    food_data = pd.DataFrame.from_records(get_item_features())
    dummy = food_data['tag'].str.get_dummies(',')
    item_feature_tags = dummy.to_sparse().to_coo()

    from sklearn.preprocessing import normalize
    item_feature_tags = normalize(item_feature_tags, axis=1, norm='l1')

    import numpy as np

    labels = np.array(dummy.columns.values)
    # item_tags, indices = np.unique(all_item_tags,return_inverse=True)

    # test_cutoff_index = int(len(interactions.data) * (1.0 - test_set_fraction))
    # test_cutoff_timestamp = np.sort(interactions.data)[test_cutoff_index]
    # in_train = interactions.data < test_cutoff_timestamp
    # in_test = np.logical_not(in_train)

    import scipy.sparse as sp

    eye = sp.eye(item_price.shape[0], item_price.shape[0]).tocsr()
    item_features_concat = sp.hstack((eye, item_price))
    offset = item_features_concat.shape[1]
    item_features_concat = sp.hstack((item_features_concat,item_feature_tags))
    item_features_concat = item_features_concat.tocsr().astype(np.float32)

    user_data = pd.DataFrame.from_records(get_user_features())
    user_feature_tags = user_data['tag'].str.get_dummies(',').to_sparse().to_coo()
    u_eye = sp.eye(user_feature_tags.shape[0], user_feature_tags.shape[0]).tocsr()
    user_features_concat = sp.hstack((u_eye,user_feature_tags))
    user_features_concat = user_features_concat.tocsr().astype(np.float32)

    interactions = sp.coo_matrix((np.ones(len(interactions.data), dtype=np.float32),
                           (interactions.row,
                            interactions.col)),
                          shape=interactions.shape)

    # train_weights = sp.coo_matrix(weights.data[in_train],
    #                        (weights.row[in_train],
    #                         weights.col[in_train])),
    #                       shape=weights.shape)

    # test = sp.coo_matrix((np.ones(in_test.sum(), dtype=np.float32),
    #                       (interactions.row[in_test],
    #                        interactions.col[in_test])),
    #                      shape=interactions.shape)

    # test_weights = sp.coo_matrix(weights.data[in_test],
    #                        (weights.row[in_test],
    #                         weights.col[in_test])),
    #                       shape=weights.shape)
    print(repr(interactions))
    print(repr(weights))
    print(repr(item_features_concat))
    print(repr(user_features_concat))

    return {
            # 'train': train,
            # 'test': test,
            # 'train_weights': train_weights,
            # 'test_weights': test_weights,
            'item_features': item_features_concat,
            'item_tag_label_offset': offset,
            'item_tag_labels': labels,
            'user_features': user_features_concat,
            'weights': weights,
            'interactions':interactions,
            'dataset': dataset,
        }

def model_suggestions():
    data = create_dataset()
    from lightfm import LightFM

    model = LightFM(loss='logistic',no_components=30,learning_rate=0.0001, item_alpha=1e-6, user_alpha=1e-2)
    model.fit_partial(data['interactions'], item_features=data['item_features'], user_features=data['user_features'], sample_weight=data['weights'], epochs=30, num_threads=2,verbose=True)

    from lightfm.evaluation import auc_score
    food_auc = auc_score(model, data['interactions'], item_features=data['item_features'], user_features=data['user_features'], num_threads=2)
    print('AUC: %s' % food_auc)

    return {
        'model': model,
        'interactions': data['interactions'],
        'item_features': data['item_features'],
        'user_features': data['user_features'],
        'dataset': data['dataset'],
    }

def get_suggestions(user_id,items):
    from baseapp.apps import aimodel

    num_users, num_items = aimodel['interactions'].shape

    import numpy as np

    print(aimodel)

    scores = aimodel['model'].predict(user_id,np.arange(num_items),num_threads=2)
    top_items = aimodel['interactions'].col[np.argsort(-scores)]

    recommended_cards = []
    for x in top_items[:items]:
            print(x)
            recommended_cards.append(Food.objects.get(id=x))

    return recommended_cards

def initialize_model():
    return model_suggestions()

def get_updated_ratings():
    from baseapp.apps import aimodel

    (interactions, weights) = aimodel['dataset'].build_interactions(((x.user_id, x.food_id, int(x.rating))
                                                        for x in get_ratings()))

    import scipy.sparse as sp
    import numpy as np

    interactions = sp.coo_matrix((np.ones(len(interactions.data), dtype=np.float32),
                           (interactions.row,
                            interactions.col)),
                          shape=interactions.shape)

    return {
        'interactions': interactions,
        'weights': weights
    }

def rating_remodel():
    data = get_updated_ratings()
    from baseapp.apps import aimodel

    aimodel['model'].fit_partial(data['interactions'], item_features=aimodel['item_features'], user_features=aimodel['user_features'], sample_weight=data['weights'], epochs=30, num_threads=2,verbose=True)
    aimodel['interactions'] = data['interactions']